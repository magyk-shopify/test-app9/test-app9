import { fetchDescriptionFromOpenAI } from "./openai";
import { fetchDescriptionFromHuggingFace } from "./huggingface";

export async function generateProductDescription(title){
    return await fetchDescriptionFromOpenAI(title);
}

export async function generateProductDescription2(title){
    return await fetchDescriptionFromHuggingFace(title);
}