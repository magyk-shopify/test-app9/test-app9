// openai.js

export async function fetchDescriptionFromOpenAI(title) {
  console.log("Fetching description for title:", title);

  const apiKey = "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";
  const response = await fetch("https://api.openai.com/v1/engines/text-davinci-003/completions", {
    method: "POST",
    headers: {
      Authorization: `Bearer ${apiKey}`,
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      model: "text-davinci-003",
      prompt: `Generate a product description for the following title: "${title}"`,
      max_tokens: 150,
    }),
  });

  if (!response.ok) {
    console.error("Error fetching description from OpenAI:", response.statusText);
    throw new Error(`Error fetching description: ${response.statusText}`);
  }

  const data = await response.json();
  console.log("Response from OpenAI:", data);

  return data.choices[0].text.trim();
}


