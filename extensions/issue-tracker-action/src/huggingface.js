// huggingface.js

export async function fetchDescriptionFromHuggingFace(title) {
    console.log("Fetching description for title:", title);
  
    const apiKey = "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";
    const response = await fetch("https://api-inference.huggingface.co/models/facebook/bart-large-cnn", {
      method: "POST",
      headers: {
        Authorization: `Bearer ${apiKey}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ inputs: title }),
    });
  
    if (!response.ok) {
      console.error("Error fetching description from Hugging Face:", response.statusText);
      throw new Error(`Error fetching description: ${response.statusText}`);
    }
  
    const data = await response.json();
    console.log("Response from Hugging Face:", data);

    if (!data || !data.generated_text) {
        throw new Error("Invalid response format");
    }
  
    return data.generated_text;
  }
  