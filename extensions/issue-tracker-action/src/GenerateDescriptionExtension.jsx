import { useCallback, useState } from "react";
import {
  reactExtension,
  useApi,
  TextField,
  AdminAction,
  Button,
  Box,
  Banner,
  TextArea,
} from "@shopify/ui-extensions-react/admin";
import { fetchDescriptionFromOpenAI } from "./openai";
import { fetchDescriptionFromHuggingFace } from "./huggingface";

const TARGET = "admin.product-details.action.render";

export default reactExtension(TARGET, () => <App />);

function App() {
  const { close, data } = useApi(TARGET);
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const onSubmit = useCallback(async () => {
    console.log("Generate button clicked. Title:", title);
    setLoading(true);
    setError(null);

    try {
      const generatedDescription = await fetchDescriptionFromOpenAI(title);
      if (generatedDescription) {
        setDescription(generatedDescription);
      } else {
        setError("Failed to generate description. The response was empty.");
      }
    } catch (err) {
      console.error("Error generating description:", err);
      setError(`An error occurred: ${err.message}. Please try again.`);
    } finally {
      setLoading(false);
    }
  }, [title]);

  return (
    <AdminAction
      title="Generate Description"
      primaryAction={<Button onPress={onSubmit}>Generate</Button>}
      secondaryAction={<Button onPress={close}>Cancel</Button>}
    >
      <TextField
        value={title}
        onChange={(val) => setTitle(val)}
        label="Title"
        maxLength={50}
      />
      <Box paddingBlockStart="large">
        {loading ? (
          <Box>Loading...</Box>
        ) : (
          <TextArea
            value={description}
            label="Generated Description"
            readOnly
          />
        )}
        {error && (
          <Box paddingBlockStart="large">
            <Banner status="critical">{error}</Banner>
          </Box>
        )}
      </Box>
    </AdminAction>
  );
}
